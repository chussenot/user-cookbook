describe user('jamie') do
  it { should exist }
  its('groups') { should eq ['jamie'] }
end

describe group('jamie') do
  it { should exist }
end

describe directory('/home/jamie') do
  it { should exist }
end
